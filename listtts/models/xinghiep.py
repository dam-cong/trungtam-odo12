# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime

class listxinghiep(models.Model):
    _name = 'xinghiep'

    name_china = fields.Char(string='Tên xí nghiệp - tiếng Hán')
    name_romaji = fields.Char(string='Tên xí nghiệp - tiếng Romaji')
    address_china = fields.Char(string='Địa chỉ làm việc-tiếng Hán(lấy từ bảng hợp đồng lương)')
    address_romaji = fields.Char(string='Địa chỉ làm việc-tiếng Romaji(Tự phiên âm kiểm tra với khách hàng và PTTT trước khi điền vào HS)')
    phone = fields.Char(string='Số điện thoại')
    fax = fields.Char(string='Số fax')
    person_japan = fields.Char(string='Tên người đại diện - Tiếng Nhât')
    person_english = fields.Char(string='Tên người đại diện - Tiếng Anh')
