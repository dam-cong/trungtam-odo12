
from odoo import models, fields, api
import datetime

class listnghiepdoan(models.Model):
    _name = 'nghiepdoan'

    name_roma = fields.Char(string='Tên viết tắt - chữ Romaji')
    name_english = fields.Char(string='Tên Tiếng Anh')
    name_china = fields.Char(string='Tên đầy đủ - chữ Hán')
    address_japan = fields.Char(string='Địa chỉ - tiếng Nhật')
    address_roma = fields.Char(string='Địa chỉ - chữ ROMAJI')
    phone = fields.Char(string='Số điện thoại')
    id_post = fields.Char(string='Mã bưu điện (bằng số)')
    id_license = fields.Char(string='Mã giấy phép')
    fax = fields.Char(string='Số fax (Nếu có)')
    position_viet = fields.Char(string='Chức vụ của người đại diện (kí hợp trong đồng) - Tiếng Việt')
    position_china = fields.Char(string='Chức vụ của người đại diện (kí hợp trong đồng) - chữ Hán')
    name_position_roma = fields.Char(string='Tên người đại diện - chữ Romaji')
    name_position_china = fields.Char(string='Tên người đại diện - chữ Hán')
    date_sign = fields.Date(string='Ngày kí hiệp định giữa nghiệp đoàn với pháp nhân')
    fee = fields.Char(string='Phí ủy thác đao tạo(Yên)')
    subsidize = fields.Char(string='Trợ cấp đào tạo tháng đầu(Yên)')
    note_japan = fields.Char(string='Ghi chú tiếng Nhật')
    note_viet = fields.Char(string='Ghi chú tiếng Việt')


