# -*- coding: utf-8 -*-

from odoo import models, fields, api
import datetime


class listtts(models.Model):
    _name = 'listtts.listtts'

    ma = fields.Char(string='Mã số')
    name = fields.Char(string='Họ và tên')
    date_birth = fields.Date(string='Ngày sinh')
    gender = fields.Char(string='Giới tính')
    address = fields.Char(string='Địa chỉ liên hệ')
    province = fields.Char(string='Tính/TP')
    phone = fields.Char(string='Số điện thoại')
    cmnd = fields.Char(string='CMND')
    idcard = fields.Char(string='Thẻ căn cước')
    date_license = fields.Date(string='Ngày cấp')
    address_license = fields.Char(string='Nơi cấp')
    status_mary = fields.Char(string='Tình trạng hôn nhân')
    level = fields.Char(string='Trình độ học vấn')
    phone_parent = fields.Char(string='Số điện thoại người thân')
    age = fields.Char(string='Tuổi', compute='_tuoi_tts')

    @api.multi
    @api.depends('date_birth')
    def _tuoi_tts(self):
        ngay_sinh = fields.Date.today()
        if self.date_birth:
            tmp = ngay_sinh.year - self.date_birth.year
            if ngay_sinh.month == self.date_birth.month:
                if ngay_sinh.day < self.date_birth.day:
                    tmp = tmp - 1
            elif ngay_sinh.month < self.date_birth.month:
                tmp = tmp - 1
            self.age = int(tmp)
