# -*- coding: utf-8 -*-
{
    'name': "listtts",
    'depends': ['base'],
    'data': [
        # 'security/ir.model.access.csv',
        'views/views.xml',
        'views/templates.xml',
        'views/nghiepdoan.xml',
        'views/xinghiep.xml',
    ],
}